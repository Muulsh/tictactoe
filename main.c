#include <stdio.h>
#include <string.h>

int posword(char word[]) {
    if      (strcmp(word, "top-left")      == 0) return 0;
    else if (strcmp(word, "top-middle")    == 0) return 1;
    else if (strcmp(word, "top-right")     == 0) return 2;
    else if (strcmp(word, "middle-left")   == 0) return 3;
    else if (strcmp(word, "middle-middle") == 0) return 4;
    else if (strcmp(word, "middle-right")  == 0) return 5;
    else if (strcmp(word, "bottom-left")   == 0) return 6;
    else if (strcmp(word, "bottom-middle") == 0) return 7;
    else if (strcmp(word, "bottom-right")  == 0) return 8;
    else return 9;
}

int checkwin(char (*game)[]) {
    return 0;
}

void getgame(char (*game)[]) {
    FILE *file;
    if ((file = fopen("./game.txt","r")) == NULL){
        file = fopen("./game.txt", "w");
    }
    fgets(*game, 10, file);
    fclose(file);
}

void savegame(char (*game)[]) {
    FILE *file;
    file = fopen("./game.txt", "w");
    fprintf(file, "%s", *game);
    fclose(file);
}

void showgame(char (*game)[]) {
    for (int i = 0; i < 9; i+=3) {
        printf(" %c | %c | %c\n", (*game)[i], (*game)[i+1], (*game)[i+2]);
    }
}

void cleargame(char (*game)[]) {
    for (int i = 0; i < 9; i++) {
        (*game)[i] = ' ';
    }
}

void playbot(char (*game)[]) {
    for (int i = 0; i < 3; ++i) {
        if                  ((*game)[i]   == 'x' && (*game)[i+3] == 'x' && (*game)[i+6] == ' ') (*game)[i+6] = 'o';
        else if             ((*game)[i]   == 'x' && (*game)[i+6] == 'x' && (*game)[i+3] == ' ') (*game)[i+3] = 'o';
        else if             ((*game)[i+3] == 'x' && (*game)[i+6] == 'x' && (*game)[i]   == ' ') (*game)[i]   = 'o';
        else if (i%3 == 0 && (*game)[i]   == 'x' && (*game)[i+1] == 'x' && (*game)[i+2] == ' ') (*game)[i+2] = 'o';
        else if (i%3 == 0 && (*game)[i]   == 'x' && (*game)[i+2] == 'x' && (*game)[i+1] == ' ') (*game)[i+1] = 'o';
        else if (i%3 == 0 && (*game)[i+1] == 'x' && (*game)[i+2] == 'x' && (*game)[i]   == ' ') (*game)[i]   = 'o';
        else if             ((*game)[0]   == 'x' && (*game)[4]   == 'x' && (*game)[8]   == ' ') { (*game)[8] = 'o'; break; }
        else if             ((*game)[0]   == 'x' && (*game)[8]   == 'x' && (*game)[4]   == ' ') { (*game)[4] = 'o'; break; }
        else if             ((*game)[4]   == 'x' && (*game)[8]   == 'x' && (*game)[0]   == ' ') { (*game)[0] = 'o'; break; }
        else if             ((*game)[2]   == 'x' && (*game)[4]   == 'x' && (*game)[6]   == ' ') { (*game)[6] = 'o'; break; }
        else if             ((*game)[2]   == 'x' && (*game)[6]   == 'x' && (*game)[4]   == ' ') { (*game)[4] = 'o'; break; }
        else if             ((*game)[4]   == 'x' && (*game)[6]   == 'x' && (*game)[2]   == ' ') { (*game)[2] = 'o'; break; }
        else { for (int j = 0; j < 9; ++j) { if((*game)[j] == ' ') { (*game)[j] = 'o'; break; } } break; }
    }
}

void play(int argc, char *argv[]) {
    if (argc < 3) {
        printf("need 2 args\n");
        return;
    }

    char game[10];
    getgame(&game);

    int mychoice = posword(argv[2]);
    if (mychoice == 9) {
        printf("unknown position\n");
        return;
    }
    game[mychoice] = 'x';

    playbot(&game);
    checkwin(&game);
    savegame(&game);
    showgame(&game);
}

int main(int argc, char *argv[]) {
    if (argc < 2) {
        printf("need 1 args\n");
        return 1;
    }
    if (strcmp(argv[1], "play") == 0) {
        play(argc, argv);
    } else if (strcmp(argv[1], "show") == 0) {
        char game[10];
        getgame(&game);
        showgame(&game);
    } else if (strcmp(argv[1], "clear") == 0) {
        char game[10];
        getgame(&game);
        cleargame(&game);
        savegame(&game);
    }
    return 0;
}
